package ru.coderiders.matcher;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.File;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class PdfMatcherTest {

    FileMatcher fileMatcher;

    @BeforeEach
    void set_up() {
//        fileMatcher = new PdfMatcher();
    }

    @Test
    void getExtensions() {
        PdfMatcher pdfMatcher = new PdfMatcher();
        Set<String> extensions = pdfMatcher.getExtensions();
        assertNotNull(extensions);
        assertEquals(1, extensions.size());
        assertTrue(extensions.contains(".pdf"));
    }
}