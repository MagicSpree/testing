package ru.coderiders.matcher;


import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

class FileMatcherTest {

    File file;
    FileMatcher fileMatcher;

    @BeforeEach
    void set_up() {
        file = mock(File.class);
        fileMatcher = new PdfMatcher();
    }


    @Test
    void match_Throw_IllegalArgumentException() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(false);
        when(file.getName()).thenReturn("name");
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () ->
                fileMatcher.match(file));
        assertEquals("File not exists: " + "name", exception.getMessage());
    }

    @Test
    void match_ResultFalse() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(true);
        fileMatcher.match(file);
        assertFalse(fileMatcher.match(file));
    }

    @Test
    void match_ResultTrue() {
        File file = mock(File.class);
        when(file.exists()).thenReturn(true);
        when(file.isDirectory()).thenReturn(false);
        when(file.getName()).thenReturn("file.pdf");
        assertTrue(fileMatcher.match(file));
    }
}