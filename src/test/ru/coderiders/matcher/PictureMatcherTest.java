package ru.coderiders.matcher;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PictureMatcherTest {

    @Test
    void getExtensions() {
        PictureMatcher pictureMatcher = new PictureMatcher();
        Set<String> extensions = pictureMatcher.getExtensions();
        assertNotNull(extensions);
        assertEquals(3, extensions.size());
        assertTrue(extensions.contains(".png"));
        assertTrue(extensions.contains(".jpg"));
        assertTrue(extensions.contains(".jpeg"));
    }
}