package ru.coderiders.scanner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.coderiders.matcher.FileMatcher;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DirectoryScannerTest {

    Path directory;
    FileMatcher matcher;

    @BeforeEach
    void init() {
        directory = mock(Path.class);
        matcher = mock(FileMatcher.class);
    }

    @Test
    void scan_Throw_IllegalArgumentException_IsNotExists() {
        DirectoryScanner directoryScanner = new DirectoryScanner(directory, matcher, false);
        File file = spy(new File("fff.pdf"));
        when(directory.toFile()).thenReturn(file);
        doReturn(false).when(file).exists();
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, directoryScanner::scan);
        assertEquals("Directory is not exists: " + directory, exception.getMessage());
    }

    @Test
    void scan_Throw_IllegalArgumentException_IsNotDirectory() {
        DirectoryScanner directoryScanner = new DirectoryScanner(directory, matcher, false);
        File file = spy(new File("main.pdf"));
        when(directory.toFile()).thenReturn(file);
        doReturn(true).when(file).exists();
        doReturn(true).when(file).isFile();
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, directoryScanner::scan);
        assertEquals("Argument is not directory: " + directory, exception.getMessage());
    }

    @Test
    void scan_Throw_IllegalArgumentException_From_ScanDir_WithHelpRuntimeException() {
        DirectoryScanner directoryScanner = spy(new DirectoryScanner(directory, matcher, false));
        File file = spy(new File("main.pdf"));
        when(directory.toFile()).thenReturn(file);
        doReturn(true).when(file).exists();
        doReturn(false).when(file).isFile();
        doThrow(new RuntimeException()).when(directoryScanner).scanDir(any(File.class));
        IllegalStateException exception = assertThrows(IllegalStateException.class, directoryScanner::scan);
        assertNotNull(exception);
    }

    @Test
    void scan_Throw_IllegalArgumentException_From_ScanDir() {
        DirectoryScanner directoryScanner = spy(new DirectoryScanner(directory, matcher, false));
        File file = spy(new File("main.pdf"));
        when(directory.toFile()).thenReturn(file);
        doReturn(true).when(file).exists();
        doReturn(false).when(file).isFile();
        doThrow(new IllegalStateException()).when(directoryScanner).scanDir(any(File.class));
        IllegalStateException exception = assertThrows(IllegalStateException.class, directoryScanner::scan);
        assertNotNull(exception);
    }

    @Test
    void scanDir_IsFileTrue() {
        DirectoryScanner directoryScanner = spy(new DirectoryScanner(directory, matcher, false));
        File file = mock(File.class);
        when(file.isFile()).thenReturn(true);
        when(matcher.match(file)).thenReturn(true);
        when(file.toPath()).thenReturn(Path.of("ff"));
        List<Path> list = directoryScanner.scanDir(file);
        assertEquals(1, list.size());
    }

    @Test
    void scanDir_DeepModeAndIsFileFalse() {
        DirectoryScanner directoryScanner = spy(new DirectoryScanner(directory, matcher, false));
        File file = mock(File.class);
        when(file.isFile()).thenReturn(false);
        File[] files = {
                new File("fff")
        };
        File fileFromArray = spy(files[0]);
        when(file.listFiles()).thenReturn(files);
        when(matcher.match(any(File.class))).thenReturn(true);
        doReturn(Path.of("ff")).when(fileFromArray).toPath();
        List<Path> list = directoryScanner.scanDir(file);
        assertEquals(1, list.size());
    }

    @Test
    void scanDir_WithRecursion() {
        DirectoryScanner directoryScanner = spy(new DirectoryScanner(directory, matcher, true));
        File file = mock(File.class);
        when(file.isFile()).thenReturn(false);
        File[] files = {
                new File("fff")
        };
        File fileFromArray = spy(files[0]);
        when(file.listFiles()).thenReturn(files);
        doReturn(Path.of("ff")).when(fileFromArray).toPath();
        when(fileFromArray.toPath()).thenReturn(Path.of("ff"));
//        doReturn(List.of(Path.of("fff"))).when(directoryScanner).scanDir(fileFromArray);
        List<Path> list = directoryScanner.scanDir(file);
        assertEquals(1, list.size());
    }
}